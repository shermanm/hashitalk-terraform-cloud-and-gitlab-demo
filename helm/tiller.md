### Installing Tiller for Helm V2
- Initialize Helm and Install Tiller in current kubectl context (`kubectl config current-context`. Assuming you are in the root directory of this repo, please run the commands below.
```
kubectl apply -f helm/helm-service-account.yaml
helm init --service-account tiller --upgrade
# Verify that the tiller-deploy pod is running
kubectl -n kube-system get po | grep tiller
```
Reference: [https://helm.sh/docs/using_helm/#initialize-helm-and-install-tiller](initialize-helm-and-install-tiller)

- Alternatively, if you have already installed helm and want to upgrade it please run the command below.
```
helm init --service-account tiller --upgrade
# Verify that the tiller-deploy pod is running
kubectl -n kube-system get po | grep tiller
```

