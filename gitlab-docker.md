### Create a GitLab Runner Docker image (Optional)
The Docker image used for the GitLab Runner in this demo has already made public in Dockerhub as [kawsark/gitlab-terraform:0.0.2](https://cloud.docker.com/repository/docker/kawsark/gitlab-terraform). This container image is capable of performing an API driven run using `curl` and `jq`. It also contains an `amd64` Vault binary which is used as a CLI to interact with Vault.

You can optionally customize a GitLab Runner image using included example [Dockerfile](scripts/Dockerfile). Below are the steps to create a custom image and upload to a container registry.
```
vault_version=1.6.1
wget https://releases.hashicorp.com/vault/${vault_version}/vault_${vault_version}_linux_amd64.zip
unzip vault_${vault_version}_linux_amd64.zip

cat <<EOF>Dockerfile
FROM ruby:2.1

# Add the Vault binary
ADD ./vault /bin/vault
RUN apt-get update -y
RUN apt-get install -y curl jq

# Add the Terraform binary for CLI driven Runs
RUN apt-get update && apt-get install -y curl unzip jq
RUN curl -s https://releases.hashicorp.com/terraform/0.12.29/terraform_0.12.29_linux_amd64.zip -o terraform.zip
RUN unzip terraform.zip
RUN cp terraform /usr/local/bin/terraform && chmod +x /usr/local/bin/terraform
EOF

# Customize Dockerfile as needed
docker build -t <your_docker_username>/gitlab-terraform:0.0.2 .
docker login
docker push <your_docker_username>/gitlab-terraform:0.0.2
```
