## Testing terraform locally
To test locally without GitLab or TFC, please install terraform 0.12 and use the following example commands. 
- Note: The GOOGLE_CREDENTIALS environment variable must be transformed to remove new lines. Please see [transforming GOOGLE_CREDENTIALS - Local](transform_gcp_creds-local.md).
```
export TF_VAR_gcp_project="your-project-name"
export GOOGLE_CREDENTIALS="$(cat /path/to/gcp-service-account.json)"
terraform init
terraform apply
```
- To destroy, please run the command: `terraform destroy`.
